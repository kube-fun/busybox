# Busybox

Simple but useful examples for busybox in K8S

## Create namespace

```bash
k apply -f namespace/namespace.yaml
k ns busybox-test
k apply -f namespace/namespace-config.yaml
```

## Run pod only

```bash
k apply -f pod/busybox.yaml
```

```bash
k delete -f pod/busybox.yaml
```

## Run as job

```bash
k apply -f ./job/busybox.yaml
```

```bash
k delete -f ./job/busybox.yaml
```

## Run as daemon-set

```bash
k apply -f ./daemonset/busybox.yaml
```

```bash
k delete -f ./daemonset/busybox.yaml
```


## Busybox sh

```bash
kubectl run -i --tty busybox --image=busybox:1.31 --restart=Never -- sh
```